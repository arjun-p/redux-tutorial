// The reducer function will receive the current store state and the dispatched action, and it should return the new state. Notice that the state in the Redux store must necessarily be treated as immutable – so be careful use one of the following constructs to represent the state:
// • Single primitive values (a String, Boolean value or a number)
// • An Array or primitive values (in. E.g.: [1,2,3,4])
// • An object of primitive values (in. E.g.: {name:’cassio’, age:35})
// • An object with nested objects that will be manipulated using React immutable helpers.
// • An immutable structure (using something like immutable.js library, for example).

import constants from './constants';

const initialState = {
  balance: 0
}

const bankReducer = (state = initialState, action) => {
  console.log(action);

  switch (action.type) {
    // case constants.CREATE_ACCOUNT:
    //   return initialState;

    case constants.DEPOSIT_INTO_ACCOUNT:
      return {balance: state.balance + parseFloat(action.amount)};

    case constants.WITHDRAW_FROM_ACCOUNT:
      return {balance: state.balance - parseFloat(action.amount)};

    default:
      return state;

  }
}

export default bankReducer;
