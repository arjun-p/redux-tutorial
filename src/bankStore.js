// In a Redux application, the store owns state and is instantiated with the reducers that manipulates that state.

import {createStore} from 'redux';
import bankReducer from './bankReducer';

const bankStore = createStore(bankReducer);
export default bankStore;
