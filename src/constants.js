// We need three constants to uniquely identify our actions across the app for creating an account, depositing in the account and withdrawing from the account

export default {
  CREATE_ACCOUNT: 'create account',
  WITHDRAW_FROM_ACCOUNT: 'withdraw from account',
  DEPOSIT_INTO_ACCOUNT: 'deposit into account'
}
