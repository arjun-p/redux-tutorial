import React from 'react';
import ReactDOM from 'react-dom';
import BankAppContainer from './App'

ReactDOM.render(
  <BankAppContainer />,
  document.getElementById('root')
);
